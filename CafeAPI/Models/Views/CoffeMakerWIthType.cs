﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace CafeAPI.Models.Views
{
    public class CoffeMakerWithType     
    {
        [Key]
        public int id { get; set; }
        public string description { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }

        public int type_id { get; set; }
        public string type_name { get; set; }
        public string type_description { get; set; }
        public string type_image { get; set; }

        List<CoffeType> coffes;

        public static list_response<CoffeMakerWithType> getAll()
        {
            list_response<CoffeMakerWithType> result = new list_response<CoffeMakerWithType>(); 
            try
            {
                using(CoffeDBContext context = new CoffeDBContext())
                {
                    result.list = (from c in context.CoffeMakerWithType select c).ToList();
                    result.status = (int)response_status_enum.ok;
                }
            }
            catch(Exception e)
            {
                result.processException(e);
            }
            return result;
        }
    }


}