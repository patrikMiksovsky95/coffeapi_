﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CafeAPI.Models
{
    public class CoffeMaker
    {
        [Key]
        public int id { get; set; }
        public string description { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int type_id { get; set; }

        public id_response insert()
        {
            id_response result = new id_response();
            try
            {
                using(CoffeDBContext context = new CoffeDBContext())
                {
                    context.CoffeMaker.Add(this);
                    context.SaveChanges();
                    result.status = (int)response_status_enum.ok;
                    result.id = this.id;
                }
            }
            catch(Exception e)
            {
                result.processException(e);
            }
            return result;
        }

        public static list_response<CoffeMaker> get()
        {
            list_response<CoffeMaker> result = new list_response<CoffeMaker>();
            try
            {
                using(CoffeDBContext context = new CoffeDBContext())
                {
                    result.list = (from c in context.CoffeMaker select c).ToList();
                    result.status = (int)response_status_enum.ok;
                }
            }
            catch(Exception e)
            {
                result.processException(e);
            }
            return result;
        }
    }
}