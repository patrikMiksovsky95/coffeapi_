﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CafeAPI.Models
{
    public class CoffeDBContext :DbContext
    {
        public CoffeDBContext() : base("DefaultConnection") { }
        public DbSet<CoffeMaker> CoffeMaker { get; set; }
        public DbSet<CoffeMakerType> CoffeMakerType { get; set; }
        public DbSet<CoffeType> CoffeType { get; set; }
        public DbSet<Relations.CoffeMakerType_CoffeType> CoffeMakerType_CoffeType { get; set; }
        public DbSet<Views.CoffeMakerWithType> CoffeMakerWithType { get; set; }
    }
}