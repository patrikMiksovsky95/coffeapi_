﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CafeAPI.Models.Relations
{
    public class CoffeMakerType_CoffeType
    {
        [Key]
        [Column(Order = 0)]
        public int CoffeMakerType_Id { get; set; }
        [Key]
        [Column(Order = 1)]
        public int CoffeType_Id { get; set; }
    }
}