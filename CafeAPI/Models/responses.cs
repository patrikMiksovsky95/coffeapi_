﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CafeAPI
{
    public class id_response : a_response
    {
        public int id { get; set; }
    }

    public class bool_response : a_response
    {
        public bool result { get; set; }
    }

    public class item_response<T> : a_response
    {
        public T item { get; set; }
    }

    public class list_response<T> : a_response
    {
        public List<T> list { get; set; }

        public list_response()
            : base()
        {
            this.list = new List<T>();
        }
    }

    public abstract class a_response
    {
        public int status { get; set; }
        public string message { get; set; }
        public string stack_trace { get; set; }

        public a_response()
        {
            this.status = (int)response_status_enum.no_result;
        }

        public void processException(Exception e)
        {
            status = (int)response_status_enum.error;
            message = e.Message;
            if (e.InnerException != null)
            {
                message += " ; innerexception: " + e.InnerException.Message;
            }
            stack_trace = e.StackTrace;
        }
    }

    public enum response_status_enum
    {
        error = -1,
        no_result = 0,
        ok = 1
    }

}