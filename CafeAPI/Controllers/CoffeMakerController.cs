﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CafeAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CoffeMakerController : ApiController
    {
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage Get()
        {
            try
            {
                list_response<Models.Views.CoffeMakerWithType> result_coffe_makers = Models.Views.CoffeMakerWithType.getAll();
                if(result_coffe_makers.status == (int)response_status_enum.ok)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result_coffe_makers.list);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result_coffe_makers);
                }
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.StackTrace);
            }
           
        }
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage Post(Models.CoffeMaker coffeMaker)
        {
            try
            {
                id_response result_save = coffeMaker.insert();
                
                if (result_save.status == (int)response_status_enum.ok)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result_save);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result_save);
                }
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.StackTrace);
            }

        }
    }

}
